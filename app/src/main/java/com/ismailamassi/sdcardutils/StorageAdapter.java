package com.ismailamassi.sdcardutils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class StorageAdapter extends RecyclerView.Adapter<StorageAdapter.ViewHolder> {
    Context context;
    List<Storage> storage;
    OnClickItem onClickItem;

    public StorageAdapter(Context context, List<Storage> storage) {
        this.context = context;
        this.storage = storage;
    }

    public OnClickItem getOnClickItem() {
        return onClickItem;
    }

    public void setOnClickItem(OnClickItem onClickItem) {
        this.onClickItem = onClickItem;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final Storage storage = this.storage.get(position);
        holder.tv_path.setText("Path: " + storage.getPath());
        holder.tv_size.setText("Free: " + Storage.formatSize(storage.getFreeSize()) + " / " + Storage.formatSize(storage.getTotalSize()));
        holder.tv_format.setText("File System: " + storage.getFormat());
        holder.btn_format.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onClickItem != null) {
                    onClickItem.onClickOnWipeData(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return storage.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_path, tv_size, tv_format;
        Button btn_format;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_path = itemView.findViewById(R.id.tv_path);
            tv_size = itemView.findViewById(R.id.tv_size);
            tv_format = itemView.findViewById(R.id.tv_format);
            btn_format = itemView.findViewById(R.id.btn_format);
        }
    }

    interface OnClickItem {
        void onClickOnWipeData(int position);
    }
}
