package com.ismailamassi.sdcardutils;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import hendrawd.storageutil.library.StorageUtil;

public class MainActivity extends AppCompatActivity implements StorageAdapter.OnClickItem {
    private static final String TAG = "MainActivity";
    private RecyclerView rvMain;
    private TextView tvCounter;
    private Button tvUpdate;
    private ArrayList<Storage> storage;
    private int counter;
    private StorageAdapter storageAdapter;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rvMain = findViewById(R.id.rv_main);
        tvCounter = findViewById(R.id.tv_counter);
        tvUpdate = findViewById(R.id.tv_update);

        storage = new ArrayList<>();

        storageAdapter = new StorageAdapter(MainActivity.this, storage);
        storageAdapter.setOnClickItem(this);
        rvMain.setLayoutManager(new LinearLayoutManager(this));
        rvMain.setHasFixedSize(true);
        rvMain.setAdapter(storageAdapter);

        updateStorageData();

        tvUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                updateStorageData();
            }
        });
    }

    private void updateStorageData() {
        storage.clear();
        counter = 0;

        String[] externalStoragePaths = StorageUtil.getStorageDirectories(this);
        for (String s : externalStoragePaths) {
            String internalPath = Environment.getExternalStorageDirectory().getAbsolutePath();
            Log.d(TAG, "onCreate: internalPath " + internalPath);
            if (!s.startsWith(internalPath)) {
                storage.add(new Storage(s, getFileSystem(s), getTotalSpace(s), getFreeSpace(s)));
                counter++;
            }
        }
        tvCounter.setText(String.valueOf(counter));
        storageAdapter.notifyDataSetChanged();
    }

    long getFreeSpace(final String path) {
        StatFs stat = new StatFs(path);
        return stat.getBlockSizeLong() * stat.getAvailableBlocksLong();
    }

    long getTotalSpace(String path) {
        StatFs stat = new StatFs(path);
        return stat.getBlockSizeLong() * stat.getBlockCountLong();
    }

    public String getFileSystem(String path) {
        try {
            Process mount = Runtime.getRuntime().exec("mount");
            BufferedReader reader = new BufferedReader(new InputStreamReader(mount.getInputStream()));
            mount.waitFor();

            String line;
            while ((line = reader.readLine()) != null) {
                String[] split = line.split("\\s+");
                for (int i = 0; i < split.length - 1; i++) {
                    if (!split[i].equals("/") && path.startsWith(split[i])) {
                        return split[i + 2];
                    }
                }
            }
            reader.close();
            mount.destroy();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }
    @Override
    public void onClickOnWipeData(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this)
                .setTitle("Wipe All data?")
                .setMessage("Are you sure you need to wipe all data on "
                        + storage.get(position).getPath() + " you can't restore data, continue?")
                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        Toast.makeText(MainActivity.this, "Deleting Data ...", Toast.LENGTH_SHORT).show();
                        Storage.wipingSdcard(storage.get(position).getPath());
                        updateStorageData();
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        dialog.dismiss();
                    }
                });
        builder.create().show();
    }
}